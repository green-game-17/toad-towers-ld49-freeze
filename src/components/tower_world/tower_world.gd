extends Spatial


const ZOOM_BASIS = 25
const ZOOM_MAX_LEVEL = 5
const CAMERA_SPEED = 25
const MAX_CAMERA_HEIGHT_MAX_ZOOM = 70
const MAX_CAMERA_HEIGHT_MIN_ZOOM = 125

const GROUND_MARGIN_BLOCKS_LR = 40
const GROUND_MARGIN_BLOCKS_B = 5
const GROUND_MARGIN_BLOCKS_F = 40


onready var rng = RandomNumberGenerator.new()
onready var is_debug_enabled = OS.has_feature('editor')

onready var camera_anchor = $CameraAnchor
onready var camera = $CameraAnchor/Camera
onready var ray_cast = $CameraAnchor/Camera/RayCast
onready var camera_size = $CameraAnchor/Camera.get_size()
onready var mouse_anchor = $MouseAnchor
onready var addon_anchor = $AddonAnchor
onready var modules_anchor = $ModulesAnchor
onready var ground_anchor = $GroundAnchor
onready var ground_block_base = $GroundAnchor/GroundBlock
onready var zone_marker_anchor = $ZoneMarkerAnchor
onready var addon_text_timer = $AddonAnchor/TextTimer
onready var addon_text = $AddonAnchor/Viewport/Control/Label

onready var module_wrapper_scene = load("res://components/tower_world/module_wrapper.tscn")
onready var zone_marker_corner_scene = load("res://components/zone_marker/zone_marker_corner.tscn")
onready var zone_marker_line_scene = load("res://components/zone_marker/zone_marker_line.tscn")


var mouse_block_x = 0
var mouse_block_y = 0
var is_mouse_valid = false
var mouse_module = null
var mouse_module_id = null
var mosue_module_component_material = null

var ground_width = 10

var zoom_level = 1
var mouse_camera = false
var mouse_moved = false
var start_pos = Vector3.ZERO
var last_mouse_pos = Vector3.ZERO
var x_position = 0

var delete_active = false
var selected_module = null


func _ready():
	rng.randomize()

	EVENTS.connect('module_selected', self, '_on_module_selected')
	EVENTS.connect('destroy_quake', self, '_on_destroy_quake')
	EVENTS.connect('destroy_wind', self, '_on_destroy_wind')
	EVENTS.connect('destroy_fire', self, '_on_destroy_fire')
	EVENTS.connect('destroy_temperature', self, '_on_destroy_temperature')
	EVENTS.connect('destroy_water', self, '_on_destroy_water')
	EVENTS.connect('destroy_snakenado', self, '_on_destroy_snakenado')
	EVENTS.connect('delete_triggered', self, '_on_delete_triggered')
	
	addon_text_timer.connect('timeout', self, '_on_text_timeout')

	__set_level(STATE.get_level_environment(), STATE.get_level_width())


func _process(delta: float):
	if Input.is_action_pressed("move_up"):
		var new_y = camera_anchor.get_translation().y + CAMERA_SPEED * zoom_level * delta
		camera_anchor.translation.y = clamp(new_y, __get_min_camera_height(), __get_max_camera_height())
	elif Input.is_action_pressed("move_down"):
		var new_y = camera_anchor.get_translation().y - CAMERA_SPEED * zoom_level * delta
		camera_anchor.translation.y = clamp(new_y, __get_min_camera_height(), __get_max_camera_height())


func _physics_process(_delta: float):
	pass


func _input(event):
	if event is InputEventMouseMotion:
		__update_mouse_position(event)
	elif Input.is_action_just_released('abort_selection'):
		EVENTS.emit_signal('module_deselected')
		mouse_module = null
		for child in mouse_anchor.get_children():
			child.queue_free()
		if not addon_text_timer.is_stopped():
			addon_text_timer.stop()
			addon_anchor.hide()
	elif Input.is_action_just_released('delete_module') and delete_active and selected_module != null:
		if STATE.has_enough_budget_to_delete():
			STATE.decrease_budget_by_delete()
			STATE.remove_module(selected_module.tower_position.x, selected_module.tower_position.y, selected_module.module_size)
			selected_module.queue_free()
			selected_module = null
			EVENTS.emit_signal('module_placed')
		else:
			__show_info_text('Not enough money')
	elif Input.is_action_just_released('confirm_placement') and not mouse_moved:
		__place_module_at_mouse()

	if Input.is_action_just_pressed('zoom_in') and zoom_level > 1:
		zoom_level -= 0.25
		zoom_level = clamp(zoom_level, 1, ZOOM_MAX_LEVEL)
		camera.size = zoom_level * ZOOM_BASIS
		camera_anchor.translation.y = clamp(
			camera_anchor.get_translation().y + last_mouse_pos.y * 0.175,
			__get_min_camera_height(),
			__get_max_camera_height()
		)
	elif Input.is_action_just_pressed('zoom_out'):
		zoom_level += 0.25
		zoom_level = clamp(zoom_level, 1, ZOOM_MAX_LEVEL)
		camera.size = zoom_level * ZOOM_BASIS
		camera_anchor.translation.y = clamp(
			camera_anchor.get_translation().y,
			__get_min_camera_height(),
			__get_max_camera_height()
		)
	
	if Input.is_action_just_pressed('mouse_camera_move'):
		mouse_camera = true
		start_pos = camera_anchor.get_translation()
	elif Input.is_action_just_released('mouse_camera_move'):
		mouse_camera = false
		if mouse_moved:
			mouse_moved = false

	if Input.is_action_just_released('pause_menu'):
		EVENTS.emit_signal('show_pause_menu')


func __set_level(environment, width):
	ground_width = width

	# ground tiles:
	var color = Color(0.2, 0.2, 0.2)
	var grass_color = Color(0.15, 0.35, 0.15)
	match environment:
		ID.GRASS:
			color = grass_color
		ID.SNOW:
			color = Color(0.85, 0.85, 0.86)
		ID.DESSERT:
			color = Color(0.8, 0.75, 0.60)
		ID.MOUNTAIN:
			color = Color(0.42, 0.42, 0.42)
		ID.WATER:
			color = Color(0.36, 0.50, 0.66)

	for iz in range(-GROUND_MARGIN_BLOCKS_B, GROUND_MARGIN_BLOCKS_F + 1):
		for ix in range(-GROUND_MARGIN_BLOCKS_LR, width + GROUND_MARGIN_BLOCKS_LR + 1):
			var new_ground_block = ground_block_base.duplicate()
			ground_anchor.add_child(new_ground_block)

			var new_material = new_ground_block.get_active_material(0).duplicate()
			new_ground_block.set_surface_material(0, new_material)
			if environment == ID.DESSERT or environment == ID.MOUNTAIN:
				new_material.albedo_color = color.lightened(rng.randf_range(0.0, 0.1))
			elif environment == ID.WATER:
				if (ix >= -1 and ix <= width + 1 and iz >= -2 and iz <= 2) or (
					ix >= -2 and ix <= width + 2 and iz >= -3 and iz <= 3 and rng.randf() < 0.6
				):
					new_material.albedo_color = grass_color.darkened(rng.randf_range(0.0, 0.1))
				else:
					new_material.albedo_color = color.lightened(rng.randf_range(0.0, 0.1))	
			else:
				new_material.albedo_color = color.darkened(rng.randf_range(0.0, 0.1))

			new_ground_block.set_translation(Vector3(
				ix * DATA.GRID_SIZE,
				0,
				iz * DATA.GRID_SIZE
			))

	ground_block_base.queue_free()

	# zone marker:
	for ix in range(1, width - 1):
		var new_line = zone_marker_line_scene.instance()
		new_line.set_translation(Vector3(ix * DATA.GRID_SIZE, 0, 0))
		zone_marker_anchor.add_child(new_line)
	var new_corner_end = zone_marker_corner_scene.instance()
	new_corner_end.set_rotation_degrees(Vector3(0, 180, 0))
	new_corner_end.set_translation(Vector3((width - 1) * DATA.GRID_SIZE, 0, 0))
	zone_marker_anchor.add_child(new_corner_end)

	# camera adjustment:
	x_position = (width - 2) / 4 * DATA.GRID_SIZE
	camera_anchor.translation.x = x_position

	if width > 10:
		zoom_level += 0.2 * (width - 10) / 2
		camera.size = zoom_level * ZOOM_BASIS
		camera_anchor.translation.y = clamp(
			camera_anchor.get_translation().y,
			__get_min_camera_height(),
			__get_max_camera_height()
		)


func _on_module_selected(id):
	for child in mouse_anchor.get_children():
		child.queue_free()

	mouse_module_id = id
	mouse_module = DATA.get_building_data(id)

	var new_component = mouse_module.component.instance()
	mouse_anchor.add_child(new_component)

	var model = new_component.get_child(0)
	mosue_module_component_material = model.get_active_material(0).duplicate()
	model.set_surface_material(0, mosue_module_component_material)

	mosue_module_component_material.flags_transparent = true
	mosue_module_component_material.albedo_color.a = 0.5
	mosue_module_component_material.albedo_color.r = 0.8
	mosue_module_component_material.albedo_color.g = 0.8
	mosue_module_component_material.albedo_color.b = 0.8
	

func _on_destroy_quake():
	for module in modules_anchor.get_children():
		module.destroy_quake()


func _on_destroy_wind():
	for module in modules_anchor.get_children():
		module.destroy_wind()


func _on_destroy_fire():
	for module in modules_anchor.get_children():
		module.destroy_fire()


func _on_destroy_temperature():
	for module in modules_anchor.get_children():
		module.destroy_temperature()


func _on_destroy_water():
	var mesh = $WaterMesh
	var t = 5.0 * 120.0
	for i in range(1.0, t):
		mesh.translation.y = lerp(-5, 5, i/t)
		if i % 60 == 0:
			mesh.rotation_degrees.z = rng.randf_range(-0.5, 0.5)
		yield(get_tree().create_timer(1.0/120.0), "timeout")


func _on_destroy_snakenado():
	$Snakenado.show()

	yield(get_tree().create_timer(5), "timeout")

	for module in modules_anchor.get_children():
		module.destroy_wind()


func _on_text_timeout():	
	addon_anchor.hide()


func __update_mouse_position(event):
	var half_window_size = get_viewport().get_size() / 2
	var percentages = event.get_global_position() - half_window_size
	half_window_size.x = half_window_size.y
	percentages /= half_window_size
	percentages *= camera_size / 2
	var y_offset = 0
	if mouse_module:
		y_offset = DATA.GRID_SIZE
	var new_pos = Vector3(percentages.x * zoom_level, -percentages.y * zoom_level - y_offset, 0)
	ray_cast.set_translation(new_pos)
	
	if not mouse_camera:
		last_mouse_pos = new_pos
	elif mouse_camera:
		var new_camera_pos = Vector3((new_pos.x - last_mouse_pos.x) * 0.7, new_pos.y - last_mouse_pos.y, 0)
		camera_anchor.translation.y = clamp(
			start_pos.y - new_camera_pos.y,
			__get_min_camera_height(),
			__get_max_camera_height()
		)
		camera_anchor.translation.x = clamp(
			start_pos.x - new_camera_pos.x,
			min(x_position, 8),
			min(x_position, 8) + max((DATA.CONTRACTS[STATE.get_selected_level()].build_area_size - 10) * 3, 0)
		)
		if new_camera_pos.length_squared() > 0.01:
			mouse_moved = true

	var collider = ray_cast.get_collider()
	if collider == null:
		return
	
	if mouse_module == null:
		if collider is RigidBody and collider.has_node('IsModule') and delete_active:
			selected_module = collider.get_parent()
		return # nope
	
	var new_anchor_pos = Vector3.ZERO

	var collision_point = ray_cast.get_collision_point()

	if collider.name == 'SkyRayCollider':
		__mark_mouse_as_invalid()
		new_anchor_pos = Vector3(
			stepify(collision_point.x, 1) + 1,
			stepify(collision_point.y, 1) - 1 + DATA.GRID_SIZE,
			0
		)

	else:
		var x_block_shift = int(mouse_module.size.x / 2)
		# quick fix, something is wrong with negative values, but idk
		if collision_point.x < DATA.GRID_SIZE / 2 + x_block_shift * DATA.GRID_SIZE:
			mouse_block_x = 0
			if mouse_module.size.x == 1:
				new_anchor_pos.x = 0
			elif mouse_module.size.x % 2 == 1:
				new_anchor_pos.x = DATA.GRID_SIZE * int(mouse_module.size.x / 2)
			else: 
				new_anchor_pos.x = DATA.GRID_SIZE * (int(mouse_module.size.x / 2) - 0.5)
		else:
			mouse_block_x = int((collision_point.x - DATA.GRID_SIZE / 2) / DATA.GRID_SIZE) - x_block_shift + 1
			if mouse_block_x + mouse_module.size.x > ground_width:
				mouse_block_x = ground_width - mouse_module.size.x

			var x_shift = 0
			if mouse_module.size.x % 2 == 0:
				x_shift = float(DATA.GRID_SIZE) / 2
			new_anchor_pos.x = mouse_block_x * DATA.GRID_SIZE - x_shift + x_block_shift * DATA.GRID_SIZE

		if collider.name == 'FloorRayCollider':
			mouse_block_y = 0
		else:
			mouse_block_y = int(collision_point.y / DATA.GRID_SIZE) + 1
			new_anchor_pos.y = mouse_block_y * DATA.GRID_SIZE

		if STATE.get_module_is_colliding(
				mouse_block_x, mouse_block_y, mouse_module.size
			) or not STATE.get_module_is_supported(
				mouse_block_x, mouse_block_y, mouse_module.size
			):
			__mark_mouse_as_invalid()
		else:
			__mark_mouse_as_valid()
	
	mouse_anchor.set_translation(new_anchor_pos)
	addon_anchor.set_translation(new_anchor_pos)


func __mark_mouse_as_invalid():
	mosue_module_component_material.albedo_color.r = 1
	is_mouse_valid = false


func __mark_mouse_as_valid():
	mosue_module_component_material.albedo_color.r = 0.8
	is_mouse_valid = true


func __place_module_at_mouse():
	if is_mouse_valid and mouse_module != null:
		if 'placement_requirements' in mouse_module:
			var place_req = mouse_module.placement_requirements
			if 'min_height' in place_req and place_req.min_height > mouse_block_y:
				__show_info_text('Can only be placed above floor %s'%[place_req.min_height])
				return
			elif 'max_height' in place_req and place_req.max_height < mouse_block_y:
				__show_info_text('Can only be placed below floor %s'%[place_req.max_height])
				return
		
		if STATE.has_enough_budget(mouse_module_id):
			STATE.add_module(mouse_module_id, mouse_block_x, mouse_block_y, mouse_module.size)
			STATE.decrease_budget(DATA.CARDS[mouse_module_id].costs)

			var new_module = mouse_module.component.instance()
			var new_module_wrapper = module_wrapper_scene.instance()
			new_module_wrapper.set_module(new_module, mouse_module.size, mouse_anchor.translation)
			new_module_wrapper.set_tower_position(mouse_block_x, mouse_block_y)
			modules_anchor.add_child(new_module_wrapper)

			EVENTS.emit_signal('module_placed')
			mouse_module = null
			for child in mouse_anchor.get_children():
				child.queue_free()
		else:
			__show_info_text('Not enough money')


func __show_info_text(text: String):
	addon_text.set_text(text)
	addon_anchor.show()
	addon_text_timer.start()


func __get_min_camera_height():
	return (zoom_level * 10) + zoom_level


func __get_max_camera_height():
	return lerp(MAX_CAMERA_HEIGHT_MIN_ZOOM, MAX_CAMERA_HEIGHT_MAX_ZOOM, zoom_level / ZOOM_MAX_LEVEL)


func _on_delete_triggered():
	delete_active = !delete_active
