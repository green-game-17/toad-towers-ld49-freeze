extends PanelContainer


signal gets_selected(index)


export var has_gui_input: bool = true
export var is_special_card: bool = false

var entry_scene = load('res://components/hud/card_stat_entry.tscn')
var stylebox = load('res://assets/styles/card_separator.tres')

var id
var data
var stack_position = 0

var is_mouse_inside = false


func _ready():
	$ClickArea.connect('mouse_entered', self, '_on_mouse_entered')
	$ClickArea.connect('mouse_exited', self, '_on_mouse_exited')
	$ClickArea.connect('gui_input', self, '_on_gui_input')
	
	if !has_gui_input:
		$ClickArea.set_mouse_filter(Control.MOUSE_FILTER_IGNORE)


func set_id(new_id: String):
	id = new_id
	data = DATA.CARDS[id]
	
	add_stylebox_override('panel', get_stylebox('panel').duplicate())
	$Margin/Panel/VBox/Category.add_stylebox_override('normal', $Margin/Panel/VBox/Category.get_stylebox('normal').duplicate())
	
	var category_data = DATA.CARD_CATEGORIES[data.category]
	$Margin/Panel/VBox/Category.set_text(category_data.name)
	get_stylebox('panel').set_bg_color(Color(1, 1, 1, 1).linear_interpolate(category_data.color, 0.1))
	$Margin/Panel/VBox/Category.get_stylebox('normal').set_bg_color(category_data.color)
	$Margin/Panel/VBox/Title.set_text(data.name)
	$Margin/Panel/VBox/Image.set_texture(data.image)
	$Margin/Panel/VBox/Stats/VBox/Description.set_text(data.description)
	$Margin/Panel/VBox/Costs/HBox/Amount.set_text(STATE.format_number(data.costs))
	
	for child in $Margin/Panel/VBox/Stats/VBox/Panel/VBox/Entries.get_children():
		child.queue_free()
		yield(child, 'tree_exited')
	
	for key in data.size:
		__add_entry(key, data.size[key])
	
	for key in data.stats:
		__add_entry(key, data.stats[key])


func __add_entry(key: String, value: float):
	if $Margin/Panel/VBox/Stats/VBox/Panel/VBox/Entries.get_child_count() > 0:
		var new_separator = HSeparator.new()
		new_separator.add_stylebox_override('separator', stylebox)
		$Margin/Panel/VBox/Stats/VBox/Panel/VBox/Entries.add_child(new_separator)
	var new_entry = entry_scene.instance()
	new_entry.set_data(key, value)
	$Margin/Panel/VBox/Stats/VBox/Panel/VBox/Entries.add_child(new_entry)


func set_stack_position(new_position: int):
	stack_position = new_position


func _on_mouse_entered():
	if has_gui_input:
		is_mouse_inside = true
		EVENTS.emit_signal('show_card_big', true, id, stack_position)


func _on_mouse_exited():
	if has_gui_input:
		is_mouse_inside = false
		EVENTS.emit_signal('show_card_big', false, '', 0)


func _on_gui_input(event: InputEvent):
	if event is InputEventMouseButton and !event.is_pressed() and is_mouse_inside and (stack_position == 3 or is_special_card):
		emit_signal('gets_selected', stack_position)
		EVENTS.emit_signal('module_selected', id)
