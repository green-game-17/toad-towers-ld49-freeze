extends Control


func _ready():
	if STATE.get_selected_level() == 0:
		show()
	else:
		hide()

	$CenterContainer/Panel/VBoxContainer/ContinueButton.connect('pressed', self, 'hide')
