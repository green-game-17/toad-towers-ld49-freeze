extends HBoxContainer


func _ready():
	pass # Replace with function body.


func set_data(key: String, value):
	match key:
		ID.REQUIRED_MODULE:
			$Label.set_text('%s modules'%[DATA.CARDS[value.type].name])
			$Value.set_text(str(value.amount))
		ID.REQUIRED_CATEGORY:
			$Label.set_text('modules of the category %s'%[DATA.CARD_CATEGORIES[value.type].name])
			$Value.set_text(str(value.amount))
		ID.SPACE:
			$Label.set_text('Space for inhabitants')
			$Value.set_text(str(value))
		ID.HEIGHT:
			$Label.set_text('Stories high')
			$Value.set_text(str(value))
		ID.ENTERTAINMENT_FACTOR:
			$Label.set_text('Entertainment score')
			$Value.set_text(str(value))
		ID.WORK:
			$Label.set_text('Space for workers')
			$Value.set_text(str(value))
