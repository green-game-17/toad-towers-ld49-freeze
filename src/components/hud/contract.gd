extends Control


var entry_scene = load('res://components/hud/contract_req_entry.tscn')


func _ready():
	$OKButton.connect('pressed', self, 'hide')
	
	var contract_data = DATA.CONTRACTS[STATE.get_selected_level()]
	$Paper/Margin/VBox/Contractor/Image.set_texture(contract_data.contractor.image)
	$Paper/Margin/VBox/Contractor/Details/Name.set_text(contract_data.contractor.name)
	$Paper/Margin/VBox/Contractor/Details/Company.set_text(contract_data.contractor.company)
	$Paper/Margin/VBox/Contractor/Details/Tel.set_text(contract_data.contractor.tel)
	$Paper/Margin/VBox/Contractor/Details/Mail.set_text(contract_data.contractor.mail)
	$Paper/Margin/VBox/Info.set_text(contract_data.details)
	for key in contract_data.min_requirements:
		var new_entry = entry_scene.instance()
		new_entry.set_data(key, contract_data.min_requirements[key])
		$Paper/Margin/VBox/MinReq.add_child(new_entry)
	for key in contract_data.bonus_requirements:
		var new_entry = entry_scene.instance()
		new_entry.set_data(key, contract_data.bonus_requirements[key])
		$Paper/Margin/VBox/BonusReq.add_child(new_entry)
	$Paper/Margin/VBox/Budget/Value.set_text(STATE.format_number(contract_data.budget))
