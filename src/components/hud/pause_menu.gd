extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	EVENTS.connect('show_pause_menu', self, 'show')
	$ContinueButton.connect('pressed', self, 'hide')
	$RestartButton.connect('pressed', self, '_on_restart_level')
	$MenuButton.connect('pressed', self, '_on_back_to_menu')


func _on_back_to_menu():
	STATE.set_show_level_select(true)
	SCENE.goto_scene('menu')


func _on_restart_level():
	STATE.set_level(STATE.get_level())
	SCENE.goto_scene('world')
