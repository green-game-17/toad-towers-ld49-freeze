extends Control


var entry_scene = load('res://components/hud/requirements_entry.tscn')


# Called when the node enters the scene tree for the first time.
func _ready():
	var contract_data = DATA.CONTRACTS[STATE.get_selected_level()]
	for key in contract_data.min_requirements:
		var new_entry = entry_scene.instance()
		new_entry.set_data(key, contract_data.min_requirements[key])
		$Requirements/HBox/Margin/MinReq.add_child(new_entry)
	for key in contract_data.bonus_requirements:
		var new_entry = entry_scene.instance()
		new_entry.set_data(key, contract_data.bonus_requirements[key])
		$Requirements/HBox/Margin2/BonusReq.add_child(new_entry)
	$Budget/Margin/HBox/Value.set_text(STATE.format_number(contract_data.budget))
	
	EVENTS.connect('module_placed', self, '_on_update_budget')


func _on_update_budget():
	$Budget/Margin/HBox/Value.set_text(STATE.format_number(STATE.get_budget()))





