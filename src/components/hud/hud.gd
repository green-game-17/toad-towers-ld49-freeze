extends CanvasLayer


var delete_active = false


# Called when the node enters the scene tree for the first time.
func _ready():
	$DeleteButton.connect('pressed', self, '_on_delete_triggered')


func _on_delete_triggered():
	delete_active = !delete_active
	EVENTS.emit_signal('delete_triggered')
	if delete_active:
		$Label.show()
	else:
		$Label.hide()

