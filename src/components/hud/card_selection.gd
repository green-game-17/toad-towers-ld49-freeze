extends Control


var selected_stack = -1
var contract_data = null
var special_cards_played = []
var special_card_visible = false


func _ready():
	randomize()
	
	EVENTS.connect('module_placed', self, '_on_module_placed')
	EVENTS.connect('module_deselected', self, '_on_module_deselected')
	EVENTS.connect('show_card_big', self, '_on_show_card_big')
	EVENTS.connect('disaster_struck', self, '_on_disaster_struck')
	
	for i in $Stacks.get_child_count():
		$Stacks.get_child(i).connect('card_selected', self, '_on_card_select', [i])
	
	contract_data = DATA.CONTRACTS[STATE.get_selected_level()]
	for i in contract_data.stacks.size():
		var child = $Stacks.get_child(i)
		child.set_category(contract_data.stacks[i])
		match contract_data.stacks.size():
			1:
				child.set_position(Vector2(228, 475))
			2:
				child.set_position(Vector2(132 + i * 192, 475))
			3:
				child.set_position(Vector2(36 + i * 192, 475))
			4:
				child.set_position(Vector2(132 + floor(i % 2) * 192, 325 + floor(i / 2.0) * 300))
			5:
				if i < 3:
					child.set_position(Vector2(36 + floor(i % 3) * 192, 325 + floor(i / 3.0) * 300))
				else:
					child.set_position(Vector2(132 + floor(i % 3) * 192, 325 + floor(i / 3.0) * 300))
			6:
				child.set_position(Vector2(36 + floor(i % 3) * 192, 325 + floor(i / 3.0) * 300))
			7:
				if i < 3:
					child.set_position(Vector2(36 + floor(i % 3) * 192, 175 + floor(i / 3.0) * 300))
				else:
					child.set_position(Vector2(132 + floor((i - 1) % 2) * 192, 175 + floor((i - 1) / 2.0) * 300))
			8:
				if i < 6:
					child.set_position(Vector2(36 + floor(i % 3) * 192, 175 + floor(i / 3.0) * 300))
				else:
					child.set_position(Vector2(132 + floor((i - 2) % 2) * 192, 175 + floor(i / 3.0) * 300))
			9:
				child.set_position(Vector2(36 + floor(i % 3) * 192, 175 + floor(i / 3.0) * 300))
		child.show()


func _on_show_card_big(is_visible: bool, id: String, stack_position: int):
	$BigCardModal.set_visible(is_visible)
	if id != '':
		$BigCardModal/Card.set_id(id)
	if stack_position == 3:
		$BigCardModal/Card.set_modulate(Color(1, 1, 1, 1))
	else:
		$BigCardModal/Card.set_modulate(Color(0.7, 0.7, 0.7, 0.9))


func _on_card_select(index: int):
	selected_stack = index


func _on_module_placed():
	if selected_stack == -1 and not special_card_visible:
		return
	if not special_card_visible:
		$Stacks.get_child(selected_stack).remove_card(3)
		selected_stack = -1
	special_card_visible = false
	$SpecialCard.hide()
	
	var tower_height = STATE.get_tower_height()
	for height in range(tower_height - 3, tower_height + 1):
		if not height in special_cards_played:
			if height in contract_data.special_cards:
				$SpecialCard/VBox/Margin/Card.set_id(contract_data.special_cards[height])
				special_cards_played.append(height)
				special_card_visible = true
				
				$SpecialCard.show()
				break
	
	if tower_height + 2 in contract_data.disasters:
		$Title.set_text('Next %s in 2 stories'%[DATA.DISASTERS[contract_data.disasters[tower_height + 2].type].name])
		$Title.get_stylebox('normal').set_bg_color(Color(0.82, 0.8, 0.31, 1))
	elif tower_height + 1 in contract_data.disasters:
		$Title.set_text('Next %s in 1 story'%[DATA.DISASTERS[contract_data.disasters[tower_height + 1].type].name])
		$Title.get_stylebox('normal').set_bg_color(Color(0.82, 0.8, 0.31, 1))
	else:
		$Title.set_text('Everything is fine')
		$Title.get_stylebox('normal').set_bg_color(Color(0.36, 0.82, 0.31, 1))


func _on_module_deselected():
	selected_stack = -1


func _on_disaster_struck(disaster_id: String):
	$Title.set_text('%s struck your building'%[DATA.DISASTERS[disaster_id].name])
	$Title.get_stylebox('normal').set_bg_color(Color(0.82, 0.31, 0.31, 1))
