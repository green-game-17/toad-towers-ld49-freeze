extends Control


const HEIGHT_MULTIPLICATOR = 50
const BUDGET_MULTIPLICATOR = 0.5
const POINTS_PER_BONUS_REQ = 100


var entry_scene = load('res://components/hud/end_screen_req_entry.tscn')
var fulfilled_bonus_requirements = 0
var level_failed = false


func _ready():
	EVENTS.connect('level_success', self, '_set_data')
	EVENTS.connect('level_failed', self, '_on_level_failed')
	
	$OKButton.connect('pressed', self, '_on_confirm')


func _on_level_failed(disaster_id: String):
	level_failed = true
	$NotFulfilled.set_text('Not fulfilled\nReason: %s'%[DATA.DISASTERS[disaster_id].name])
	_set_data()


func _set_data():
	var contract_data = DATA.CONTRACTS[STATE.get_selected_level()]
	var remaining_budget = STATE.get_budget()
	var win_value = STATE.calc_happiness_score() + STATE.get_tower_height() * HEIGHT_MULTIPLICATOR + remaining_budget * BUDGET_MULTIPLICATOR
	
	$Paper/Margin/VBox/Contractor/Image.set_texture(contract_data.contractor.image)
	$Paper/Margin/VBox/Contractor/Details/Name.set_text(contract_data.contractor.name)
	$Paper/Margin/VBox/Contractor/Details/Company.set_text(contract_data.contractor.company)
	$Paper/Margin/VBox/Contractor/Details/Tel.set_text(contract_data.contractor.tel)
	$Paper/Margin/VBox/Contractor/Details/Mail.set_text(contract_data.contractor.mail)
	for key in contract_data.min_requirements:
		var new_entry = entry_scene.instance()
		new_entry.set_data(key, contract_data.min_requirements[key])
		$Paper/Margin/VBox/MinReq.add_child(new_entry)
	for key in contract_data.bonus_requirements:
		var new_entry = entry_scene.instance()
		new_entry.set_data(key, contract_data.bonus_requirements[key])
		$Paper/Margin/VBox/BonusReq.add_child(new_entry)
		fulfilled_bonus_requirements += __check_requirement(key, contract_data.bonus_requirements[key])
	win_value += fulfilled_bonus_requirements * POINTS_PER_BONUS_REQ
	$Paper/Margin/VBox/Budget/BudgetSpent/Value.set_text('-' + STATE.format_number(contract_data.budget - STATE.get_budget()))
	$Paper/Margin/VBox/Budget/BudgetRemaining/Value.set_text(STATE.format_number(remaining_budget))
	
	if not level_failed:
		var lilypads = 1
		if win_value >= contract_data.min_win_values[3] and fulfilled_bonus_requirements == contract_data.bonus_requirements.size() and STATE.get_discarded_cards() == 0:
			$Paper/Margin/VBox/Rating/HBox/Lilypad3.set_self_modulate(Color(1, 1, 1, 1))
			lilypads += 1
		if win_value >= contract_data.min_win_values[2]:
			$Paper/Margin/VBox/Rating/HBox/Lilypad2.set_self_modulate(Color(1, 1, 1, 1))
			lilypads += 1
		$Paper/Margin/VBox/Rating/HBox/Lilypad1.set_self_modulate(Color(1, 1, 1, 1))
		STATE.add_level_result(str(STATE.get_level()), lilypads)
		STATE.unlock_level(STATE.get_level() + 2)
	else:
		$NotFulfilled.show()

	$Paper/Margin/VBox/ScoreLabel.set_text(STATE.format_number(win_value))

	show()


func __check_requirement(key: String, value):
	var new_value = 0
	match key:
		ID.REQUIRED_MODULE:
			new_value = STATE.get_single_module_count(value.type)
		ID.REQUIRED_CATEGORY:
			new_value = STATE.get_module_category_count(value.type)
		ID.SPACE:
			new_value = STATE.get_module_category_value(ID.LIVING_SPACE, ID.SPACE)
		ID.HEIGHT:
			new_value = STATE.get_tower_height()
		ID.ENTERTAINMENT_FACTOR:
			new_value = STATE.get_module_category_value(ID.ENTERTAINMENT, ID.ENTERTAINMENT_FACTOR)
		ID.WORK:
			new_value = STATE.get_module_category_value(ID.OFFICE_SPACE, ID.WORK)
	
	if (value is Dictionary and new_value >= value.amount) or (value is int and new_value >= value):
		return 1
	return 0


func _on_confirm():
	STATE.set_show_level_select(true)
	SCENE.goto_scene('menu')
