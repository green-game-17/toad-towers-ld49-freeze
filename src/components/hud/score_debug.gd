extends Control


onready var height_value = $Panel/GridContainer/HeightValue
onready var stability_score = $Panel/GridContainer/StabilityScore
onready var happiness_score = $Panel/GridContainer/HapinessScroe

onready var is_enabled = OS.has_feature('editor')


func _ready():
	hide()

	EVENTS.connect('module_placed', self, '_on_refresh')


func _input(_event):
	if is_enabled && Input.is_action_just_pressed("debug_f9"):
		show()
		_on_refresh()


func _on_refresh():
	height_value.set_text(str(STATE.get_tower_height()))
	stability_score.set_text(str(STATE.calc_stability_score()))
	happiness_score.set_text(str(STATE.calc_happiness_score()))
