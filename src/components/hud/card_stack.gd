extends Control


signal card_selected()


const CARD_SPACE = 35

var card_scene = load('res://components/hud/card.tscn')
var category
var card_ids = []
var current_ids = []


func _ready():
	randomize()


func set_category(id: String):
	category = id
	card_ids = DATA.get_card_ids_by_category(category)
	if get_child_count() > 0:
		for child in get_children():
			child.queue_free()
		current_ids = []
	for i in 4:
		add_card()


func add_card():
	var id
#	while not id in current_ids:
	id = card_ids[randi() % card_ids.size()]
	for index in get_child_count():
		var child = get_child(index)
		child.set_position(Vector2(0, (index + 1) * CARD_SPACE))
		child.set_stack_position(index + 1)
		if index + 1 == 3:
			child.set_modulate(Color(1, 1, 1, 1))
	var new_child = card_scene.instance()
	new_child.set_id(id)
	current_ids.push_front(id)
	new_child.set_scale(Vector2(0.24, 0.24))
	new_child.set_modulate(Color(0.7, 0.7, 0.7, 0.9))
	new_child.connect('gets_selected', self, '_on_card_selected')
	add_child(new_child)
	move_child(new_child, 0)


func _on_card_selected(_stack_index: int):
	emit_signal('card_selected')


func remove_card(index: int):
	get_children()[index].queue_free()
	current_ids.remove(index)
	add_card()
