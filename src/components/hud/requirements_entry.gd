extends HBoxContainer


var id
var max_value
var type


func _ready():
	EVENTS.connect('module_placed', self, '_on_update_value')


func set_data(new_id: String, value):
	match new_id:
		ID.REQUIRED_MODULE:
			$CheckBox.set_text('%s %s modules'%[value.amount, DATA.CARDS[value.type].name])
		ID.REQUIRED_CATEGORY:
			$CheckBox.set_text('%s modules of the category %s'%[value.amount, DATA.CARD_CATEGORIES[value.type].name])
		ID.SPACE:
			$CheckBox.set_text('Space for %s inhabitants'%[value])
		ID.HEIGHT:
			$CheckBox.set_text('%s stories high'%[value])
		ID.ENTERTAINMENT_FACTOR:
			$CheckBox.set_text('An entertainment score of %s'%[value])
		ID.WORK:
			$CheckBox.set_text('Space for %s workers'%[value])
	
	if value is float or value is int:
		$ProgressBar.set_max(value)
		max_value = value
	else:
		$ProgressBar.set_max(value.amount)
		max_value = value.amount
		type = value.type
	id = new_id


func _on_update_value():
	var new_value = 0
	match id:
		ID.REQUIRED_MODULE:
			new_value = STATE.get_single_module_count(type)
		ID.REQUIRED_CATEGORY:
			new_value = STATE.get_module_category_count(type)
		ID.SPACE:
			new_value = STATE.get_module_category_value(ID.LIVING_SPACE, ID.SPACE)
		ID.HEIGHT:
			new_value = STATE.get_tower_height()
		ID.ENTERTAINMENT_FACTOR:
			new_value = STATE.get_module_category_value(ID.ENTERTAINMENT, ID.ENTERTAINMENT_FACTOR)
		ID.WORK:
			new_value = STATE.get_module_category_value(ID.OFFICE_SPACE, ID.WORK)
	
	$ProgressBar.set_value(new_value)
	if new_value >= max_value:
		$CheckBox.set_pressed(true)
