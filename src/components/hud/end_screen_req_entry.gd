extends HBoxContainer


var max_value
var type


func _ready():
	pass # Replace with function body.


func set_data(new_id: String, value):
	match new_id:
		ID.REQUIRED_MODULE:
			$CheckBox.set_text('%s modules'%[DATA.CARDS[value.type].name])
		ID.REQUIRED_CATEGORY:
			$CheckBox.set_text('modules of the category %s'%[DATA.CARD_CATEGORIES[value.type].name])
		ID.SPACE:
			$CheckBox.set_text('Space for inhabitants')
		ID.HEIGHT:
			$CheckBox.set_text('Stories high')
		ID.ENTERTAINMENT_FACTOR:
			$CheckBox.set_text('Entertainment score')
		ID.WORK:
			$CheckBox.set_text('Space for workers')
	
	if value is float or value is int:
		$ProgressBar.set_max(value)
		max_value = value
	else:
		$ProgressBar.set_max(value.amount)
		max_value = value.amount
		type = value.type
	__set_value(new_id)


func __set_value(id: String):
	var new_value = 0
	match id:
		ID.REQUIRED_MODULE:
			new_value = STATE.get_single_module_count(type)
		ID.REQUIRED_CATEGORY:
			new_value = STATE.get_module_category_count(type)
		ID.SPACE:
			new_value = STATE.get_module_category_value(ID.LIVING_SPACE, ID.SPACE)
		ID.HEIGHT:
			new_value = STATE.get_tower_height()
		ID.ENTERTAINMENT_FACTOR:
			new_value = STATE.calc_happiness_score()
		ID.WORK:
			new_value = STATE.get_module_category_value(ID.OFFICE_SPACE, ID.WORK)
	
	$ProgressBar.set_value(new_value)
	if new_value >= max_value:
		$CheckBox.set_pressed(true)
