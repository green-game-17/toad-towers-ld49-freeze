extends Node


const SCORE_PRECISION_MULTIPLIER = 100
const COSTS_FOR_MODULE_DELETE = 1000


# debug
var is_debug = false


# Mutable, shared state data:
var __level_module_blocks = []
var __level_environment = ID.GRASS
var __level_width = 10
var __selected_level = 0
var __budget = 0
var __discarded_cards = 0
var __show_level_select = false

var __level_results = {}
var __unlocked_levels = [1]


func _init():
	reset_all()

	
func reset_all():
	reset_level(0, ID.GRASS, 10)


func reset_level(selected, environment, width):
	__level_module_blocks = [[]]
	__level_module_blocks[0].resize(width + 1)

	__selected_level = selected
	__level_environment = environment
	__level_width = width


func set_level(selected):
	var level_data = DATA.get_level_data(selected)
	reset_level(selected, level_data.area, level_data.build_area_size)
	set_budget(level_data.budget)


func get_level() -> int:
	return __selected_level


func set_show_level_select(show_it: bool):
	__show_level_select = show_it


func get_show_level_select() -> bool:
	return __show_level_select


func is_level_unlocked(level_number: int) -> bool:
	return level_number in __unlocked_levels


func unlock_level(level_number: int):
	if not level_number in __unlocked_levels:
		__unlocked_levels.append(level_number)


func get_unlocked_levels() -> Array:
	return __unlocked_levels


func reset_unlocked_levels():
	__unlocked_levels = [1]


func inc_discarded_cards():
	__discarded_cards += 1


func get_discarded_cards() -> int:
	return __discarded_cards


func add_module(id, x, y, size) -> void:
	assert(x >= 0 and y >= 0, '[STATE]: add_module: x or y invalid, x=%d, y=%d'%[x, y])

	if __level_module_blocks.size() < y + size.y:
		var old_size = __level_module_blocks.size()
		__level_module_blocks.resize(y + size.y)
		for iy in range(old_size, y + size.y):
			__level_module_blocks[iy] = []
			__level_module_blocks[iy].resize(__level_width + 1)

	for iy in range(y, y + size.y):
		for ix in range(x, x + size.x):
			__level_module_blocks[iy][ix] = id

	if is_debug:
		print('new tower:')
		for iy in __level_module_blocks.size():
			var iiy = __level_module_blocks.size() - iy - 1
			var row = []
			for ix in __level_module_blocks[iiy].size():
				row.append('.' if (__level_module_blocks[iiy][ix]) == null else '#')
			print('  %d: %s'%[iiy, row])


func remove_module(x, y, size):
	for iy in range(y, y + size.y):
		for ix in range(x, x + size.x):
			__level_module_blocks[iy][ix] = null

	if is_debug:
		print('new tower:')
		for iy in __level_module_blocks.size():
			var iiy = __level_module_blocks.size() - iy - 1
			var row = []
			for ix in __level_module_blocks[iiy].size():
				row.append('.' if (__level_module_blocks[iiy][ix]) == null else '#')
			print('  %d: %s'%[iiy, row])


func get_module_is_colliding(x, y, size) -> bool:
	if __level_module_blocks.size() < y:
		return false

	for iy in range(y, min(__level_module_blocks.size(), y + size.y)):
		for ix in range(x, x + size.x):
			if __level_module_blocks[iy][ix] != null:
				return true

	return false


func get_module_is_supported(x, y, size) -> bool:
	if y == 0:
		return true
	
	if y > __level_module_blocks.size():
		return false

	for ix in range(x, x + size.x):
		if __level_module_blocks[y - 1][ix] != null:
			return true

	return false


func get_level_environment() -> String:
	return __level_environment


func get_level_width() -> int:
	return __level_width


func get_tower_height() -> int:
	return __level_module_blocks.size()


func get_selected_level() -> int:
	return __selected_level


func set_budget(value: int):
	__budget = value


func decrease_budget(value: int):
	__budget -= value


func get_budget() -> int:
	return __budget


func has_enough_budget(id: String) -> bool:
	return DATA.CARDS[id].costs <= __budget


func has_enough_budget_to_delete() -> bool:
	return COSTS_FOR_MODULE_DELETE <= __budget


func decrease_budget_by_delete():
	__budget -= COSTS_FOR_MODULE_DELETE


func add_level_result(level_number: String, lilypads: int):
	__level_results[level_number] = lilypads


func get_level_result(level_number: String) -> int:
	if level_number in __level_results:
		return __level_results[level_number]
	return 0


func get_level_results() -> Dictionary:
	return __level_results.duplicate(true)


func reset_level_results():
	__level_results = {}


func format_number(count: int) -> String:
	if count >= 1_000_000_000:
		return 'way too much'
	elif count >= 1_000_000:
		return '%s %03d %03d' % [
			floor(count / 1_000_000), floor((count % 1_000_000) / 1_000), count % 1_000
		]
	elif count >= 1_000:
		return '%s %03d' % [floor(count / 1_000), count % 1_000]
	else:
		return str(count)


func calc_stability_score() -> int:
	var count_bad = 0
	var count_good = 0

	for iy in range(1, __level_module_blocks.size()):
		for ix in range(0, __level_width + 1):
			if __level_module_blocks[iy][ix] != null:
				if __level_module_blocks[iy - 1][ix] != null:
					count_good += 1
				else:
					count_bad += 1
	
	return to_score(float(max(1, count_good)) / float(max(1, count_bad)))


func calc_module_category_share_score(category_id: String) -> int:
	var count = get_module_category_count(category_id)
	var total = get_total_blocks_count()
	if count == 0:
		return 0
	return to_score(count / total)


func calc_specific_modules_share_score(ids: Array) -> int:
	var count = get_multiple_modules_count(ids)
	var total = get_total_blocks_count()
	if count == 0:
		return 0
	return to_score(count / total)


func calc_happiness_score() -> int:
	var entertainment_value = get_module_category_value(ID.ENTERTAINMENT, ID.ENTERTAINMENT_FACTOR) * 10
	var living_value = get_module_category_value(ID.LIVING_SPACE, ID.SPACE)
	if entertainment_value == 0 or living_value == 0:
		return 0
	return to_score(entertainment_value / living_value)


func calc_drowning_is_safe(safe_height: int) -> bool:
	for iy in range(0, min(safe_height, __level_module_blocks.size())):
		for block in __level_module_blocks[iy]:
			if block != null:
				return false
	return true
	


func get_module_category_count(category_id: String) -> float:
	var module_ids = DATA.get_card_ids_by_category(category_id)
	return get_multiple_modules_count(module_ids)
	

func get_module_category_value(category_id: String, stat_id: String) -> float:
	var total_value = 0.0
	for id in DATA.get_card_ids_by_category(category_id):
		var type_value = DATA.get_building_data(id).stats.get(stat_id, null)
		if type_value != null:
			total_value += get_single_module_count(id) * type_value
	return total_value


func get_multiple_modules_count(ids: Array) -> float:
	var count = 0.0
	for id in ids:
		count += get_single_module_count(id)
	return count


func get_single_module_count(id: String) -> float:
	var count = get_blocks_count(id)
	var module_data = DATA.get_building_data(id)
	var module_size = module_data.size.x * module_data.size.y
	if count == 0:
		return 0.0
	return count / module_size


func get_blocks_count(id: String) -> float:
	var count = 0.0

	for row in __level_module_blocks:
		for block in row:
			if block == id:
				count += 1

	return count


func get_total_blocks_count() -> float:
	var count = 0.0

	for row in __level_module_blocks:
		for block in row:
			if block != null:
				count += 1

	return count


func to_score(score: float) -> int:
	if score < 0:
		score = 0

	return int(score * SCORE_PRECISION_MULTIPLIER)
