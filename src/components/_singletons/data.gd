extends Node

# Static, shared data:
const GRID_SIZE = 3

var CARDS = {
	ID.SMALL_APARTMENT: {
		'category': ID.LIVING_SPACE,
		'name': "Small Apartment",
		'image': load('res://assets/card_icons/frog_apartment.png'),
		'component': load('res://components/modules/appartment2.tscn'),
		'description': 'There can do be people living here, so get in and enjoy',
		'stats': {
			ID.SPACE: 2
		},
		'size': {
			'x': 2,
			'y': 1
		},
		'costs': 5_000
	},
	ID.STANDARD_APARTMENT: {
		'category': ID.LIVING_SPACE,
		'name': "Standard Apartment",
		'image': load('res://assets/card_icons/frog_apartment.png'),
		'component': load('res://components/modules/appartment3.tscn'),
		'description': "An apartment for a normal sized family",
		'stats': {
			ID.SPACE: 4
		},
		'size': {
			'x': 3,
			'y': 1
		},
		'costs': 8_000
	},
	ID.BIG_APARTMENT: {
		'category': ID.LIVING_SPACE,
		'name': "Big Apartment",
		'image': load('res://assets/card_icons/frog_apartment.png'),
		'component': load('res://components/modules/appartment4.tscn'),
		'description': "A luxury apartment for up to 10 people, or for having a big party!",
		'stats': {
			ID.SPACE: 10
		},
		'size': {
			'x': 3,
			'y': 2
		},
		'costs': 18_000
	},
	ID.POOL: {
		'category': ID.ENTERTAINMENT,
		'name': "Pool",
		'image': load('res://assets/card_icons/Frog_PoolCard.png'),
		'component': load('res://components/modules/pool3.tscn'),
		'description': "You can swim here, until you fall out of the window. After that you can swim no more.",
		'stats': {
			ID.ENTERTAINMENT_FACTOR: 14
		},
		'size': {
			'x': 3,
			'y': 2
		},
		'costs': 25_000
	},
	ID.BAR: {
		'category': ID.ENTERTAINMENT,
		'name': "Bar",
		'image': load('res://assets/card_icons/Frog_BarCard.png'),
		'component': load('res://components/modules/bar2.tscn'),
		'description': "You drink and have fun with friends or alone with depression. It's your choice",
		'stats': {
			ID.ENTERTAINMENT_FACTOR: 8,
			ID.WORK: 1
		},
		'size': {
			'x': 2,
			'y': 1
		},
		'costs': 12_000
	},
	ID.SMALL_GARDEN: {
		'category': ID.ENTERTAINMENT,
		'name': "Garden",
		'image': load('res://assets/card_icons/Frog_GardenCard.png'),
		'component': load('res://components/modules/garden2.tscn'),
		'description': "A small garden to find your inner peace and to cultivate the plants you want to eat tomorrow.",
		'stats': {
			ID.ENTERTAINMENT_FACTOR: 7,
			ID.WORK: 2
		},
		'size': {
			'x': 2,
			'y': 1
		},
		'costs': 8_000
	},
	ID.SMALL_GYM: {
		'category': ID.ENTERTAINMENT,
		'name': "Gym",
		'image': load('res://assets/card_icons/Frog_GymCard.png'),
		'component': load('res://components/modules/gym2.tscn'),
		'description': "Here you can work out and get that nice body you always dreamt of having. But only if you're a frog",
		'stats': {
			ID.ENTERTAINMENT_FACTOR: 10
		},
		'size': {
			'x': 3,
			'y': 1
		},
		'costs': 12_000
	},
	ID.SPA: {
		'category': ID.ENTERTAINMENT,
		'name': "Spa",
		'image': load('res://assets/card_icons/Frog_SpaCard.png'),
		'component': load('res://components/modules/spa3.tscn'),
		'description': "Get a load off...relax....connect your X-BOX account with Discord....relax....",
		'stats': {
			ID.ENTERTAINMENT_FACTOR: 20,
			ID.WORK: 2
		},
		'size': {
			'x': 3,
			'y': 1
		},
		'costs': 23_000
	},
	ID.VIEWING_LOUNGE: {
		'category': ID.ENTERTAINMENT,
		'name': "Viewing Lounge",
		'image': load('res://assets/card_icons/viewing_platform.png'),
		'component': load('res://components/modules/viewing4.tscn'),
		'description': "Some frogs are afraid of heights, but they will have to deal with it.",
		'stats': {
			ID.ENTERTAINMENT_FACTOR: 50
		},
		'size': {
			'x': 4,
			'y': 1
		},
		'costs': 35_000
	},
	ID.SERVICE: {
		'category': ID.OFFICE_SPACE,
		'name': "Service",
		'image': load('res://assets/card_icons/service_frog.png'),
		'component': load('res://components/modules/service1.tscn'),
		'description': "You need something? Get it here while it lasts!",
		'stats': {
			ID.WORK: 1
		},
		'size': {
			'x': 1,
			'y': 1
		},
		'costs': 1_500
	},
	ID.SMALL_OFFICE: {
		'category': ID.OFFICE_SPACE,
		'name': "Office",
		'image': load('res://assets/card_icons/frog_office.png'),
		'component': load('res://components/modules/office2.tscn'),
		'description': "Working is fun and you should do it right now or else you're fired",
		'stats': {
			ID.WORK: 2
		},
		'size': {
			'x': 2,
			'y': 1
		},
		'costs': 8_000
	},
	ID.STORE: {
		'category': ID.OFFICE_SPACE,
		'name': "Store",
		'image': load('res://assets/card_icons/Frog_MallCard.png'),
		'component': load('res://components/modules/store2.tscn'),
		'description': "Frogs need to get their basic stuff for life, you know?",
		'stats': {
			ID.ENTERTAINMENT_FACTOR: 20,
			ID.WORK: 2
		},
		'size': {
			'x': 2,
			'y': 1
		},
		'costs': 15_000
	},
	ID.WEIGHTS: {
		'category': ID.DISASTER_PREVENTION,
		'name': "Small Weights",
		'image': load('res://assets/card_icons/weights.png'),
		'component': load('res://components/modules/weight1.tscn'),
		'description': "It does thingy thongys to prevent the tower collapsing if an earthquake hits it",
		'stats': {
			ID.EARTHQUAKE_RESISTANCE: 40
		},
		'size': {
			'x': 1,
			'y': 1
		},
		'placement_requirements': {
			'min_height': 0,
			'max_height': 4
		},
		'costs': 2_500
	},
	ID.BIG_WEIGHTS: {
		'category': ID.DISASTER_PREVENTION,
		'name': "Big Weights",
		'image': load('res://assets/card_icons/weights.png'),
		'component': load('res://components/modules/weight1x3.tscn'),
		'description': "It does thingy thongys to prevent the tower collapsing if an earthquake hits it",
		'stats': {
			ID.EARTHQUAKE_RESISTANCE: 100,
			ID.WIND_RESISTANCE: 50
		},
		'size': {
			'x': 1,
			'y': 3
		},
		'costs': 10_000
	},
	ID.SMALL_TEMPERATURE: {
		'category': ID.DISASTER_PREVENTION,
		'name': "Small Air-Conditioning",
		'image': load('res://assets/card_icons/ac.png'),
		'component': load('res://components/modules/ac1.tscn'),
		'description': "If your frogs feel too warm or too cold, they wont be happy.",
		'stats': {
			ID.TEMPERATURE_RESISTANCE: 40
		},
		'size': {
			'x': 1,
			'y': 1
		},
		'costs': 3_000
	},
	ID.BIG_TEMPERATURE: {
		'category': ID.DISASTER_PREVENTION,
		'name': "Big Air-Conditioning",
		'image': load('res://assets/card_icons/ac.png'),
		'component': load('res://components/modules/ac2.tscn'),
		'description': "Your residents need fresh air and they need it now!",
		'stats': {
			ID.TEMPERATURE_RESISTANCE: 200
		},
		'size': {
			'x': 2,
			'y': 2
		},
		'costs': 15_000
	},
	ID.BUNKER: {
		'category': ID.DISASTER_PREVENTION,
		'name': "Bunker",
		'image': load('res://assets/card_icons/bunker.png'),
		'component': load('res://components/modules/bunker1.tscn'),
		'description': "You need to protect your residents from any snakes, that may come your way.",
		'stats': {
			ID.SNAKENADO_RESISTANCE: 40
		},
		'size': {
			'x': 1,
			'y': 1
		},
		'costs': 5_000
	},
	ID.ARMORY: {
		'category': ID.DISASTER_PREVENTION,
		'name': "Armory",
		'image': load('res://assets/card_icons/armoury.png'),
		'component': load('res://components/modules/armory2.tscn'),
		'description': "If protection isn't enough for your residents, then you need to arm them.",
		'stats': {
			ID.SNAKENADO_RESISTANCE: 100
		},
		'size': {
			'x': 2,
			'y': 1
		},
		'costs': 18_000
	},
	ID.SMALL_FIRE: {
		'category': ID.DISASTER_PREVENTION,
		'name': "Small Water Tank",
		'image': load('res://assets/card_icons/sprinkler.png'),
		'component': load('res://components/modules/fire1.tscn'),
		'description': "To power your sprinklers, you will need some water, don't you?",
		'stats': {
			ID.FIRE_RESISTANCE: 40
		},
		'size': {
			'x': 1,
			'y': 1
		},
		'costs': 3_500
	},
	ID.BIG_FIRE: {
		'category': ID.DISASTER_PREVENTION,
		'name': "Big Water Tank",
		'image': load('res://assets/card_icons/sprinkler.png'),
		'component': load('res://components/modules/fire2.tscn'),
		'description': "More water equals less fire, isn't it?",
		'stats': {
			ID.FIRE_RESISTANCE: 200
		},
		'size': {
			'x': 1,
			'y': 2
		},
		'costs': 15_000
	},
	ID.REINFORCEMENT: {
		'category': ID.DISASTER_PREVENTION,
		'name': "Reinforcement",
		'image': load('res://assets/card_icons/sprinkler.png'),
		'component': load('res://components/modules/reinforcement1.tscn'),
		'description': "We need steel beams to make it more stable.",
		'stats': {
			ID.WIND_RESISTANCE: 100,
			ID.EARTHQUAKE_RESISTANCE: 40,
		},
		'size': {
			'x': 1,
			'y': 2
		},
		'costs': 7_500
	},
	ID.SMALL_WATER_PUMP: {
		'category': ID.DISASTER_PREVENTION,
		'name': "Small Water Pump",
		'image': load('res://assets/card_icons/sand.png'),
		'component': load('res://components/modules/water1.tscn'),
		'description': "If there is some water, this will get rid off it.",
		'stats': {
			ID.WATER_RESISTANCE: 40
		},
		'size': {
			'x': 1,
			'y': 1
		},
		'costs': 4_000
	},
	ID.BIG_WATER_PUMP: {
		'category': ID.DISASTER_PREVENTION,
		'name': "Big Water Pump",
		'image': load('res://assets/card_icons/sand.png'),
		'component': load('res://components/modules/water2.tscn'),
		'description': "If there is much water, this will definitely get rid off it.",
		'stats': {
			ID.WATER_RESISTANCE: 250
		},
		'size': {
			'x': 2,
			'y': 2
		},
		'costs': 24_000
	},
	ID.ROOFING_TILE: {
		'category': ID.ROOFING,
		'name': "Flat Roof",
		'image': load('res://assets/card_icons/roof.png'),
		'component': load('res://components/modules/roof1.tscn'),
		'description': "You need to seal the top.",
		'stats': {},
		'size': {
			'x': 1,
			'y': 1
		},
		'costs': 2_000
	},
	ID.ROOFING_WINDOW: {
		'category': ID.ROOFING,
		'name': "Flat Roof Window",
		'image': load('res://assets/card_icons/roof.png'),
		'component': load('res://components/modules/roof2.tscn'),
		'description': "If you need some light, just put in a window.",
		'stats': {},
		'size': {
			'x': 1,
			'y': 1
		},
		'costs': 3_000
	},
	ID.ROOFING_DOOR: {
		'category': ID.ROOFING,
		'name': "Flat Roof Access",
		'image': load('res://assets/card_icons/roof.png'),
		'component': load('res://components/modules/roof3.tscn'),
		'description': "Some of the frogs, like to puff some reed up there.",
		'stats': {},
		'size': {
			'x': 1,
			'y': 1
		},
		'costs': 5_000
	}
}

const CARD_CATEGORIES = {
	ID.DISASTER_PREVENTION: {
		'name': "Disaster Prevention",
		'color': Color(1, 0.24, 0.24, 1)
	},
	ID.LIVING_SPACE: {
		'name': "Living Space",
		'color': Color(0.24, 0.24, 1, 1)
	},
	ID.ENTERTAINMENT: {
		'name': "Entertainment",
		'color': Color(0.89, 0.67, 0.21, 1)
	},
	ID.OFFICE_SPACE: {
		'name': "Office Space",
		'color': Color(0.06, 0.51, 0.11, 1)
	},
	ID.ROOFING: {
		'name': "Roofing",
		'color': Color(0.32, 0.24, 0.05, 1)
	}
}

const CARD_STATS = {
	ID.EARTHQUAKE_RESISTANCE: {
		'name': "Earthquake Resistance",
		'icon': 'assets/icons/wobbly_thingy.png'
	},
	ID.TEMPERATURE_RESISTANCE: {
		'name': "Temperature Resistance",
		'icon': 'assets/icons/wobbly_thingy.png'
	},
	ID.FIRE_RESISTANCE: {
		'name': "Fire Resistance",
		'icon': 'assets/icons/wobbly_thingy.png'
	},
	ID.WIND_RESISTANCE: {
		'name': "Wind Resistance",
		'icon': 'assets/icons/wobbly_thingy.png'
	},
	ID.WATER_RESISTANCE: {
		'name': "Water Resistance",
		'icon': 'assets/icons/wobbly_thingy.png'
	},
	ID.SNAKENADO_RESISTANCE: {
		'name': "Snakenado Resistance",
		'icon': 'assets/icons/wobbly_thingy.png'
	},
	ID.SPACE: {
		'name': "Space",
		'icon': 'assets/icons/spaaaaaace.png'
	},
	ID.ENTERTAINMENT_FACTOR: {
		'name': "Entertainment Factor",
		'icon': 'assets/icons/entertainment_factor.png'
	},
	ID.WIDTH: {
		'name': "Width",
		'icon': 'assets/dunno.png'
	},
	ID.HEIGHT: {
		'name': "Height",
		'icon': 'assets/yeah.png'
	},
	ID.WORK: {
		'name': "Work",
		'icon': 'assets/sometimemaybe.png'
	}
}

var CONTRACTS = [
	{
#		Level 1
		'contractor': {
			'image': load('res://assets/card_icons/frog_logo.png'),
			'name':  "Anthony Hopkins",
			'company': "Tadpole Investment LLC",
			'tel': '00088866642069',
			'mail': 'ahop@t-investment.frog'
		},
		'details': "Hey yo, I want you to build me a very nice apartement complex, with some space for people to live there. To keep it clean you need to add a service room too. Also it's supposed to be in an area that's prone to earthquakes, so please take care of that as well. Thanks",
		'area': ID.GRASS,
		'build_area_size': 5,
		'min_requirements': {
			ID.HEIGHT: 5,
			ID.SPACE: 20,
			ID.REQUIRED_MODULE: {
				'type': ID.SERVICE,
				'amount': 1
			},
		},
		'bonus_requirements': {
			ID.SPACE: 30,
		},
		'stacks': [
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.OFFICE_SPACE,
			ID.ROOFING
		],
		'budget': 85_000,
		'special_cards': {
			3: ID.SERVICE
		},
		'disasters': {
			5: {
				'type': ID.EARTHQUAKE,
				'strength': 500
			},
			'end': {
				'type': ID.EARTHQUAKE,
				'strength': 1000
			}
		},
		'min_win_values': {
			2: 1000,
			3: 3000
		}
	},
	{
#		Level 2
		'contractor': {
			'image': load('res://assets/card_icons/frog_logo.png'),
			'name':  "Lili Pads",
			'company': "Hippity Hoppety Property",
			'tel': '01008054646',
			'mail': 'lili.pads@hhc.frog'
		},
		'details': "Hello, we from HHC want you to build us a tower for our employees. It should also include some offices, in order to allow for homeoffice. Please pay attention to the area. It is said that there are many earthquakes around here.",
		'area': ID.GRASS,
		'build_area_size': 5,
		'min_requirements': {
			ID.HEIGHT: 7,
			ID.SPACE: 32,
			ID.REQUIRED_MODULE: {
				'type': ID.SMALL_OFFICE,
				'amount': 2
			}
		},
		'bonus_requirements': {
			ID.SPACE: 40,
			ID.REQUIRED_MODULE: {
				'type': ID.SERVICE,
				'amount': 2
			}
		},
		'stacks': [
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.OFFICE_SPACE,
			ID.DISASTER_PREVENTION,
			ID.ROOFING
		],
		'budget': 125_000,
		'special_cards': {
			3: ID.SMALL_OFFICE
		},
		'disasters': {
			7: {
				'type': ID.EARTHQUAKE,
				'strength': 800
			},
			'end': {
				'type': ID.EARTHQUAKE,
				'strength': 1600
			}
		},
		'min_win_values': {
			2: 1000,
			3: 3000
		}
	},
	{
#		Level 3
		'contractor': {
			'image': load('res://assets/card_icons/frog_logo.png'),
			'name':  "Jake Hopper",
			'company': "Lake Pond Inc.",
			'tel': '0123879546',
			'mail': 'jakeboy05@lake-pond.frog'
		},
		'details': "Hey, Jake Hopper here. You need to design a modern tower for our company. It needs space for at least 35 residents, and please give them something to do. It would be real nice, if you could somehow fit a spa for me and my family.",
		'area': ID.GRASS,
		'build_area_size': 5,
		'min_requirements': {
			ID.HEIGHT: 8,
			ID.SPACE: 35,
			ID.REQUIRED_CATEGORY: {
				'type': ID.ENTERTAINMENT,
				'amount': 3
			},
		},
		'bonus_requirements': {
			ID.SPACE: 45,
			ID.REQUIRED_MODULE: {
				'type': ID.SPA,
				'amount': 1
			}
		},
		'stacks': [
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.ENTERTAINMENT,
			ID.OFFICE_SPACE,
			ID.DISASTER_PREVENTION,
			ID.ROOFING
		],
		'budget': 140_000,
		'special_cards': {
			
		},
		'disasters': {
			4: {
				'type': ID.EARTHQUAKE,
				'strength': 700
			},
			'end': {
				'type': ID.EARTHQUAKE,
				'strength': 1500
			}
		},
		'min_win_values': {
			2: 1000,
			3: 3000
		}
	},
	{
#		Level 4
		'contractor': {
			'image': load('res://assets/card_icons/frog_logo.png'),
			'name':  "Pepe Flyswatter",
			'company': "Riverside Living Initiative",
			'tel': '080033388822',
			'mail': 'pepe@rl-i.frog'
		},
		'details': "The Riverside Living Initative wants you to design a living complex, in which there are at least 3 service modules. Additionally there should be a viewing platform, but i will tell you where we want it. Also pay attention to the harsh winds and the possible fires in this region.",
		'area': ID.DESSERT,
		'build_area_size': 6,
		'min_requirements': {
			ID.HEIGHT: 8,
			ID.SPACE: 40,
			ID.REQUIRED_MODULE: {
				'type': ID.SERVICE,
				'amount': 3
			},
		},
		'bonus_requirements': {
			ID.SPACE: 55,
			ID.REQUIRED_MODULE: {
				'type': ID.VIEWING_LOUNGE,
				'amount': 1
			}
		},
		'stacks': [
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.ENTERTAINMENT,
			ID.OFFICE_SPACE,
			ID.OFFICE_SPACE,
			ID.DISASTER_PREVENTION,
			ID.ROOFING
		],
		'budget': 200_000,
		'special_cards': {
			6: ID.VIEWING_LOUNGE
		},
		'disasters': {
			4: {
				'type': ID.TORNADO,
				'strength': 150
			},
			'end': {
				'type': ID.INFERNO,
				'strength': 500
			}
		},
		'min_win_values': {
			2: 1000,
			3: 3000
		}
	},
	{
#		Level 5
		'contractor': {
			'image': load('res://assets/card_icons/frog_logo.png'),
			'name':  "Toad Tadpole",
			'company': "Tadpole Future Fond",
			'tel': '05506480659',
			'mail': 'tt@tff.frog'
		},
		'details': "Please design us a tower for our upcoming generation with at least 50 spaces, but better would be 65. They additionally need 5 entertainment modules for their freetime. There has to be a roof on every tile.",
		'area': ID.DESSERT,
		'build_area_size': 6,
		'min_requirements': {
			ID.HEIGHT: 8,
			ID.SPACE: 50,
			ID.REQUIRED_CATEGORY: {
				'type': ID.ENTERTAINMENT,
				'amount': 5
			},
		},
		'bonus_requirements': {
			ID.SPACE: 65,
			ID.REQUIRED_CATEGORY: {
				'type': ID.ROOFING,
				'amount': 6
			}
		},
		'stacks': [
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.ENTERTAINMENT,
			ID.OFFICE_SPACE,
			ID.DISASTER_PREVENTION,
			ID.ROOFING
		],
		'budget': 250_000,
		'special_cards': {
			6: ID.VIEWING_LOUNGE
		},
		'disasters': {
			5: {
				'type': ID.TORNADO,
				'strength': 150
			},
			'end': {
				'type': ID.INFERNO,
				'strength': 500
			}
		},
		'min_win_values': {
			2: 1000,
			3: 3000
		}
	},
	{
#		Level 6
		'contractor': {
			'image': load('res://assets/card_icons/frog_logo.png'),
			'name':  "Ekans Arbok",
			'company': "Evil Snake Corp",
			'tel': '6666666666666',
			'mail': 'evil@evil-snake-corp.snake'
		},
		'details': "We at Evil Snake Corp need a new headquarter. So if you could design a tower with at least 20 workspaces and if there is a viewing platform at the top that would be nice too.",
		'area': ID.DESSERT,
		'build_area_size': 6,
		'min_requirements': {
			ID.HEIGHT: 13,
			ID.REQUIRED_CATEGORY: {
				'type': ID.OFFICE_SPACE,
				'amount': 20
			},
		},
		'bonus_requirements': {
			ID.SPACE: 30,
			ID.REQUIRED_MODULE: {
				'type': ID.VIEWING_LOUNGE,
				'amount': 1
			}
		},
		'stacks': [
			ID.OFFICE_SPACE,
			ID.OFFICE_SPACE,
			ID.OFFICE_SPACE,
			ID.ENTERTAINMENT,
			ID.LIVING_SPACE,
			ID.DISASTER_PREVENTION,
			ID.ROOFING
		],
		'budget': 300_000,
		'special_cards': {
			9: ID.VIEWING_LOUNGE
		},
		'disasters': {
			4: {
				'type': ID.INFERNO,
				'strength': 200
			},
			'end': {
				'type': ID.INFERNO,
				'strength': 320
			}
		},
		'min_win_values': {
			2: 1000,
			3: 3000
		}
	},
	{
#		Level 7
		'contractor': {
			'image': load('res://assets/card_icons/frog_logo.png'),
			'name':  "Lili Pads",
			'company': "Hippity Hoppety Property",
			'tel': '01008054646',
			'mail': 'lili.pads@hhc.frog'
		},
		'details': "Hello, thanks for our last tower. This time we want a temperature resistant building, so you need to use air conditioning. We also want our residents to be able to buy everything they need in the tower, so we need at least 5 stores. Most of our residents are big families, so having big appartments would be best.",
		'area': ID.SNOW,
		'build_area_size': 8,
		'min_requirements': {
			ID.HEIGHT: 15,
			ID.SPACE: 95,
			ID.REQUIRED_MODULE: {
				'type': ID.STORE,
				'amount': 5
			},
		},
		'bonus_requirements': {
			ID.SPACE: 120,
			ID.REQUIRED_MODULE: {
				'type': ID.BIG_APARTMENT,
				'amount': 8
			}
		},
		'stacks': [
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.ENTERTAINMENT,
			ID.OFFICE_SPACE,
			ID.OFFICE_SPACE,
			ID.DISASTER_PREVENTION,
			ID.DISASTER_PREVENTION,
			ID.ROOFING
		],
		'budget': 500_000,
		'special_cards': {
			13: ID.BIG_APARTMENT
		},
		'disasters': {
			7: {
				'type': ID.COLDNESS,
				'strength': 80
			},
			'end': {
				'type': ID.COLDNESS,
				'strength': 320
			}
		},
		'min_win_values': {
			2: 1000,
			3: 3000
		}
	},
	{
#		Level 8
		'contractor': {
			'image': load('res://assets/card_icons/frog_logo.png'),
			'name':  "Bill Croats",
			'company': "none",
			'tel': '01008054646',
			'mail': 'bill@croats.frog'
		},
		'details': "Please build me a big tower. I need some security for possible intruders and i want at least 3 viewing lounges throughout. Also please make it, so I don't freeze to death. I heard the lot i bought was very unstable so you need to consider building earthquakeproof. Thanks.",
		'area': ID.SNOW,
		'build_area_size': 6,
		'min_requirements': {
			ID.HEIGHT: 15,
			ID.REQUIRED_MODULE: {
				'type': ID.ARMORY,
				'amount': 3
			},
			ID.REQUIRED_CATEGORY: {
				'type': ID.OFFICE_SPACE,
				'amount': 8
			}
		},
		'bonus_requirements': {
			ID.REQUIRED_CATEGORY: {
				'type': ID.ENTERTAINMENT,
				'amount': 8
			},
			ID.REQUIRED_MODULE: {
				'type': ID.BUNKER,
				'amount': 3
			}
		},
		'stacks': [
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.ENTERTAINMENT,
			ID.OFFICE_SPACE,
			ID.OFFICE_SPACE,
			ID.DISASTER_PREVENTION,
			ID.DISASTER_PREVENTION,
			ID.ROOFING
		],
		'budget': 500_000,
		'special_cards': {
			
		},
		'disasters': {
			5: {
				'type': ID.COLDNESS,
				'strength': 80
			},
			10: {
				'type': ID.EARTHQUAKE,
				'strength': 5000
			},
			'end': {
				'type': ID.COLDNESS,
				'strength': 320
			}
		},
		'min_win_values': {
			2: 1000,
			3: 3000
		}
	},
	{
#		Level 9
		'contractor': {
			'image': load('res://assets/card_icons/frog_logo.png'),
			'name':  "Benji Bloato",
			'company': "Bloato Building Co.",
			'tel': '051358435645',
			'mail': 'b.bloato@bbc.frog'
		},
		'details': "I need a slim tower, because my lot is not that big. But I want it very high. It should be a business only building, so no apartments. We are in a very cold area, so make it nice and cozy. Also, because it is that high, it needs some countermeasures for earthquakes and tornados.",
		'area': ID.SNOW,
		'build_area_size': 4,
		'min_requirements': {
			ID.HEIGHT: 20,
			ID.WORK: 60,
			ID.REQUIRED_MODULE: {
				'type': ID.VIEWING_LOUNGE,
				'amount': 1
			}
		},
		'bonus_requirements': {
			ID.WORK: 70,
			ID.REQUIRED_CATEGORY: {
				'type': ID.ENTERTAINMENT,
				'amount': 3
			},
		},
		'stacks': [
			ID.ENTERTAINMENT,
			ID.ENTERTAINMENT,
			ID.OFFICE_SPACE,
			ID.OFFICE_SPACE,
			ID.DISASTER_PREVENTION,
			ID.DISASTER_PREVENTION,
			ID.ROOFING
		],
		'budget': 450_000,
		'special_cards': {
			
		},
		'disasters': {
			8: {
				'type': ID.COLDNESS,
				'strength': 80
			},
			13: {
				'type': ID.EARTHQUAKE,
				'strength': 200
			},
			18: {
				'type': ID.TORNADO,
				'strength': 200
			},
			'end': {
				'type': ID.COLDNESS,
				'strength': 500
			}
		},
		'min_win_values': {
			2: 1000,
			3: 3000
		}
	},
	{
#		Level 10
		'contractor': {
			'image': load('res://assets/card_icons/frog_logo.png'),
			'name':  "Reed Richards",
			'company': "Fantastic Frog",
			'tel': '051358435645',
			'mail': 'reedster@fantastic.frog'
		},
		'details': "We need an entertainment complex on this mountain, so please use many of the entertainmanet modules. Keep the heavy winds in mind, the area is known for. Some offices would be nice too.",
		'area': ID.MOUNTAIN,
		'build_area_size': 15,
		'min_requirements': {
			ID.HEIGHT: 5,
			ID.REQUIRED_CATEGORY: {
				'type': ID.ENTERTAINMENT,
				'amount': 25
			}
		},
		'bonus_requirements': {
			ID.WORK: 35,
			ID.REQUIRED_MODULE: {
				'type': ID.SMALL_OFFICE,
				'amount': 5
			},
		},
		'stacks': [
			ID.ENTERTAINMENT,
			ID.ENTERTAINMENT,
			ID.ENTERTAINMENT,
			ID.OFFICE_SPACE,
			ID.OFFICE_SPACE,
			ID.OFFICE_SPACE,
			ID.DISASTER_PREVENTION,
			ID.DISASTER_PREVENTION,
			ID.ROOFING
		],
		'budget': 450_000,
		'special_cards': {
			
		},
		'disasters': {
			4: {
				'type': ID.TORNADO,
				'strength': 200
			},
			'end': {
				'type': ID.TORNADO,
				'strength': 320
			}
		},
		'min_win_values': {
			2: 1000,
			3: 3000
		}
	},
	{
#		Level 11
		'contractor': {
			'image': load('res://assets/card_icons/frog_logo.png'),
			'name':  "Jojo Hoppit",
			'company': "Frogger Housing Corp",
			'tel': '0524621684324',
			'mail': 'hoppit@frogger.frog'
		},
		'details': "We just need you to build an apartment complex, but we don't have that much money. Also take care of the winds and earthquakes around here.",
		'area': ID.MOUNTAIN,
		'build_area_size': 10,
		'min_requirements': {
			ID.LIVING_SPACE: 100,
		},
		'bonus_requirements': {
			ID.LIVING_SPACE: 115,
		},
		'stacks': [
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.DISASTER_PREVENTION,
			ID.DISASTER_PREVENTION,
			ID.DISASTER_PREVENTION,
			ID.ENTERTAINMENT,
			ID.OFFICE_SPACE,
			ID.ROOFING
		],
		'budget': 250_000,
		'special_cards': {
			
		},
		'disasters': {
			4: {
				'type': ID.TORNADO,
				'strength': 200
			},
			'end': {
				'type': ID.EARTHQUAKE,
				'strength': 5000
			}
		},
		'min_win_values': {
			2: 1000,
			3: 3000
		}
	},
	{
#		Level 12
		'contractor': {
			'image': load('res://assets/card_icons/frog_logo.png'),
			'name':  "Yoshi",
			'company': "Frog VPN",
			'tel': '03221621634321',
			'mail': 'yoshi@fvpn.frog'
		},
		'details': "We need a new office location. The mointain is the best region because of the natural winds, which can cool the servers. For the tower we need as many offices as possible.",
		'area': ID.MOUNTAIN,
		'build_area_size': 6,
		'min_requirements': {
			ID.HEIGHT: 12,
			ID.REQUIRED_MODULE: {
				'type': ID.SMALL_OFFICE,
				'amount': 12
			},
		},
		'bonus_requirements': {
			ID.REQUIRED_MODULE: {
				'type': ID.BIG_TEMPERATURE,
				'amount': 2
			},
		},
		'stacks': [
			ID.OFFICE_SPACE,
			ID.OFFICE_SPACE,
			ID.OFFICE_SPACE,
			ID.DISASTER_PREVENTION,
			ID.DISASTER_PREVENTION,
			ID.ENTERTAINMENT,
			ID.ROOFING
		],
		'budget': 300_000,
		'special_cards': {
			
		},
		'disasters': {
			7: {
				'type': ID.TORNADO,
				'strength': 160
			},
			'end': {
				'type': ID.EARTHQUAKE,
				'strength': 5000
			}
		},
		'min_win_values': {
			2: 1000,
			3: 3000
		}
	},
	{
#		Level 13
		'contractor': {
			'image': load('res://assets/card_icons/frog_logo.png'),
			'name':  "Hip Hop",
			'company': "Offshore Appartment Inc",
			'tel': '35432196065510',
			'mail': 'hip.hop@oai.frog'
		},
		'details': "I need an apartment building for the island, but beware, the island is very prone to floods.",
		'area': ID.WATER,
		'build_area_size': 5,
		'min_requirements': {
			ID.HEIGHT: 13,
			ID.LIVING_SPACE: 80
		},
		'bonus_requirements': {
			
		},
		'stacks': [
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.DISASTER_PREVENTION,
			ID.DISASTER_PREVENTION,
			ID.ENTERTAINMENT,
			ID.ROOFING
		],
		'budget': 320_000,
		'special_cards': {
			
		},
		'disasters': {
			3: {
				'type': ID.FLOOD,
				'strength': 40
			},
			10: {
				'type': ID.FLOOD,
				'strength': 240
			},
			'end': {
				'type': ID.FLOOD,
				'strength': 420
			}
		},
		'min_win_values': {
			2: 1000,
			3: 3000
		}
	},
	{
#		Level 14
		'contractor': {
			'image': load('res://assets/card_icons/frog_logo.png'),
			'name':  "Lili Pads",
			'company': "Hippity Hoppety Property",
			'tel': '01008054646',
			'mail': 'lili.pads@hhc.frog'
		},
		'details': "On the island there needs to be an entertainment tower. Please design it, so we can put a viewing lounge on the top. You need to pay attention to the stability because floods, tornados and earthquakes are common in this area.",
		'area': ID.WATER,
		'build_area_size': 4,
		'min_requirements': {
			ID.HEIGHT: 18,
			ID.REQUIRED_CATEGORY: {
				'type': ID.ENTERTAINMENT,
				'amount': 15
			},
		},
		'bonus_requirements': {
			ID.REQUIRED_MODULE: {
				'type': ID.SERVICE,
				'amount': 5
			},
		},
		'stacks': [
			ID.ENTERTAINMENT,
			ID.ENTERTAINMENT,
			ID.ENTERTAINMENT,
			ID.OFFICE_SPACE,
			ID.OFFICE_SPACE,
			ID.DISASTER_PREVENTION,
			ID.DISASTER_PREVENTION,
			ID.ROOFING
		],
		'budget': 450_000,
		'special_cards': {
			
		},
		'disasters': {
			5: {
				'type': ID.FLOOD,
				'strength': 40
			},
			11: {
				'type': ID.TORNADO,
				'strength': 200
			},
			'end': {
				'type': ID.EARTHQUAKE,
				'strength': 5000
			}
		},
		'min_win_values': {
			2: 1000,
			3: 3000
		}
	},
	{
#		Level 15
		'contractor': {
			'image': load('res://assets/card_icons/frog_logo.png'),
			'name':  "Ekans Arbok",
			'company': "Evil Snake Corp",
			'tel': '6666666666666',
			'mail': 'evil@evil-snake-corp.snake'
		},
		'details': "You will need a fort for the defence of your residents. We will come every fifth floor and with increasing numbers each time. So be prepared!",
		'area': ID.WATER,
		'build_area_size': 8,
		'min_requirements': {
			ID.HEIGHT: 30,
			ID.LIVING_SPACE: 100,
			ID.REQUIRED_MODULE: {
				'type': ID.ARMORY,
				'amount': 15
			},
		},
		'bonus_requirements': {
			ID.REQUIRED_MODULE: {
				'type': ID.BUNKER,
				'amount': 20
			},
		},
		'stacks': [
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.LIVING_SPACE,
			ID.DISASTER_PREVENTION,
			ID.DISASTER_PREVENTION,
			ID.DISASTER_PREVENTION,
			ID.DISASTER_PREVENTION,
			ID.DISASTER_PREVENTION,
			ID.DISASTER_PREVENTION,
		],
		'budget': 1_000_000,
		'special_cards': {
			
		},
		'disasters': {
			5: {
				'type': ID.SNAKENADO,
				'strength': 200
			},
			10: {
				'type': ID.SNAKENADO,
				'strength': 500
			},
			15: {
				'type': ID.SNAKENADO,
				'strength': 1000
			},
			20: {
				'type': ID.SNAKENADO,
				'strength': 1100
			},
			25: {
				'type': ID.SNAKENADO,
				'strength': 1500
			},
			'end': {
				'type': ID.SNAKENADO,
				'strength': 2000
			}
		},
		'min_win_values': {
			2: 1000,
			3: 3000
		}
	},
]

const DISASTERS = {
	ID.EARTHQUAKE: {
		'name': "Earthquake"
	},
	ID.TORNADO: {
		'name': "Tornado"
	},
	ID.INFERNO: {
		'name': "Inferno"
	},
	ID.SNAKENADO: {
		'name': 'Snakenado'
	},
	ID.COLDNESS: {
		'name': 'Freezing Cold'
	},
	ID.FLOOD: {
		'name': 'Flooding'
	}
}

var card_ids_by_category = {}


func _ready():
	for category in CARD_CATEGORIES:
		card_ids_by_category[category] = []
	for card_id in CARDS:
		card_ids_by_category[CARDS[card_id].category].append(card_id)


func get_card_ids_by_category(category: String) -> Array:
	return card_ids_by_category[category]


func get_building_data(id):
	return CARDS[id]


func get_level_data(i):
	return CONTRACTS[i]
